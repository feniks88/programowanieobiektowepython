import hashlib

class User:
    def __init__(self, username,password,is_logged=False):
        self.username = username
        self.password = self._encrypt_password(password)
        self.is_logged = is_logged

    def _encrypt_password(self, text):
        text = str.encode(self.username + text)
        h_text = hashlib.sha256(text)
        return h_text.hexdigest()

    def check_password(self, text):
        return self._encrypt_password(text) == self.password


class AuthenticException(Exception):
    pass
class PermissionError(Exception):
    pass
class IncorrectPassword(AuthenticException):
    pass
class IncorrectUsername(AuthenticException):
    pass
class NotLoggedError(AuthenticException):
    pass
class PasswordTooShort(AuthenticException):
    pass
class UsernameAlreadyExists(AuthenticException):
    pass
class NotPermittedError(AuthenticException):
    pass

class Authenticator:
    def __init__(self):
        self.users = {}

    def add_user(self, username, password):
        try:
            value = self.users[username]
        except(KeyError):
            raise UsernameAlreadyExists()
        else:
            if self.users[password] < 7 :
                raise PasswordTooShort
            else:
                self.users[username] = User (username, password)

    def login(self, username, password):
        try:
            user = self.users[username]
        except(KeyError):
            raise IncorrectUsername()
        else:

            if(user.check_password()):
                raise IncorrectPassword
            else:
                user.is_logged = True

    def is_logged_in(self, username):

        if username in self.users:
            return self.users[username].is_logged
        else:
            False
class Authorizor:
    def __init__(self, authenticator):
        self.permissions = {}
        self.authenticator = authenticator

    def add_permission(self, permission):
        try:
            self.permissions[permission]
        except(KeyError):
            self.permission[permission] = set()
        else:
            raise PermissionError

    def permit_user(self, username, permission):
        try:
            # zbior gwarantuje unikalnosc elementow
            perm = self.permissions[permission]
        except(KeyError):
            raise PermissionError
        else:
            if not username in self.authenticator.users:
                raise IncorrectUsername
            else:
                perm.add(username)

    def 



example = User("monika", "haslo")


