import math
from array import array

class Vector2D:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)
        
    def __iter__(self):
        return (e for e in (self.x,self.y))
    
    def __repr__(self):
        return '{0}({1!r}, {2!r})'.format(type(self).__name__, *self)  

    def __str__(self):
        return str(tuple(self)) # bo wektor jest iterowalny

    def __bytes__(self):
        return (bytes([ord(Vector2D.typecode)]) +
                bytes(array(Vector2D.typecode, self)))

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.hypot(self.x, self.y)  

    def __bool__(self):
        return bool(abs(self))  


v1 = Vector2D(4,5)
v2 = Vector2D(0,0)
print('repr(v1) ->',repr(v1))
print('bool(v1) ->',bool( v1))
print('bool(v2) ->',bool(v2))
print('abs(v1) ->',abs(v1))
print('v1 == v2 ->',v1 == v2)
print('v1 != v2 ->',v1 != v2)
print('v1 == Vector2D(4,5) ->',v1 == Vector2D(4,5))
print('v1 == (4,5) ->',v1 == (4,5))
print('(4,5) == v1 ->',(4,5) == v1)

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



