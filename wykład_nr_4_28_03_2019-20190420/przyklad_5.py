import math
from array import array

class Vector2D:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)
        
    def __iter__(self):
        return (e for e in (self.x,self.y))
    
    def __repr__(self):
        return '{0}({1!r}, {2!r})'.format(type(self).__name_, *self)  

    def __str__(self):
        return str(tuple(self))  

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(array(self.typecode, self)))  

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.hypot(self.x, self.y)  

    def __bool__(self):
        return bool(abs(self))  

    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)


v1 = Vector2D(2.5,3.5)
print("v1 ->", v1)
v1bytes = bytes(v1)
print(v1bytes)
v2 = Vector2D.frombytes(v1bytes)
print("v2 ->",v2)
print("v1 id ->", id(v1))
print("v2 id ->", id(v2))


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



