import math, reprlib
from array import array

class Vektor:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, components):
        self._components = array(self.typecode, components)
      
    def __iter__(self):
        return iter(self._components)
    
    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1] # usuwanie 'd' i koncowego naiwasu
        return '{}{}'.format(type(self).__name__, components)

    def __str__(self):
        return str(tuple(self))

    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        return self._components[index]

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(self._components))  

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.sqrt(sum(x ** 2 for x in self))  

    def __bool__(self):
        return bool(abs(self))  

    
    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)



v1 = Vektor(range(7))
print('v1 ->',v1)
print('(v1[1],v1[4]) ->',(v1[1],v1[4]))
print('v1[1:4] ->',v1[1:4])




input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



