# jak działa wycinanie
class A:
    def __getitem__(self, index):
        return index


a = A()

print('a[1] - >', a[1])
print('a[8] - >', a[8])
print('a[1:5] - >', a[1:5])
print('a[1:7:4] - >', a[1:7:4])
print('a[1:] - >', a[1:])
print('a[1:5:2,1] - >', a[1:5:2,1])
print('a[1:5:2,1:7] - >', a[1:5:2,1:7])
print()
print(dir(slice))
print(help(slice.indices))
s = 'DOMEK'
print('s ->',s)
print('s[:20:2] ->',s[:20:2])
print('s[-7:8] ->',s[-7:8])
print('slice(None,20,2).indices(len(s)) ->',slice(None,20,2).indices(len(s)))
print('slice(-7,8,None).indices(len(s)) ->',slice(-7,8,None).indices(len(s)))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



