import math, reprlib, numbers, functools
from array import array

class Vektor:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, components):
        self._components = array(self.typecode, components)
      
    def __iter__(self):
        return iter(self._components)
    
    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1] # usuwanie 'd' i koncowego naiwasu
        return '{}{}'.format(type(self).__name__, components)

    def __str__(self):
        return str(tuple(self))

    def __len__(self):
        return len(self._components)

    def __getitem__(self, index):
        if isinstance(index, slice):
            return type(self)(self._components[index])
        elif isinstance(index, numbers.Integral):
            return self._components[index]
        else:
            raise TypeError(f'{type(self)} indeksy muszą być liczbami całkowitymi')

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(self._components))  

    def __eq__(self, other):
        if len(self) != len(other):
            return False
        for n, m in zip(self,other):
            if n != m:
                return False
        return True

    def __hash__(self):
        hashes = (hash(x) for x in self._components)
        return functools.reduce(lambda a, b: a^b, hashes, 0)

    def __abs__(self):
        return math.sqrt(sum(x ** 2 for x in self))  

    def __bool__(self):
        return bool(abs(self))  

    fields = 'xyzt' # x <-> Vektor()[0], y <-> Vektor()[1] itd.

    def __getattr__(self, name):
        if len(name) == 1: 
            index = type(self).fields.find(name) 
            if 0 <= index < len(self._components):  
                return self._components[index]
        text = f'Obiekt {type(self).__name__!r} nie ma atrybutu {name!r}'  
        raise AttributeError(text)

    def __setattr__(self, name, value):
        if len(name) == 1:  
            if name in type(self).fields:  
                raise AttributeError(f'{name!r} - tylko do odczytu')
            elif name.islower():  
                raise AttributeError(f"nie można ustwić atrybutu nazwach\
od 'a' do 'z' w klasie {type(self).__name__!r}")
        super().__setattr__(name, value)    # w pozostałych przypadkach
                                            # niech __setattr__ działa jak dla
                                            # klasy bazowej   
    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)


v1 = Vektor(range(1,12,2))
v2 = Vektor(range(1,14,2))
print('v1 ->', v1)
print('v2 ->', v2)
print('v1 == v2',v1 == v2)
s = set()
s.add(v1)
s.add(v2)
print(s)




input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



