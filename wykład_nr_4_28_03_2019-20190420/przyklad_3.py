# porównanie dekoratorów @classmethod i @staticmethod

class A:
    @staticmethod
    def method_st(*args): # przyjmuje dowolną liczbę argumentów pozycyjnych
        return args       # zwraca argumenty pozycyjne

    @classmethod
    def method_cl(*args):
        return args

    def method(*args):
        return args

print(A.method_cl())
print(A.method_cl('arg1', 'arg2'))
print()
print(A.method_st())
print(A.method_st('arg1', 'arg2'))
print()
print(A().method())
print(A().method('arg1', 'arg2'))


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



