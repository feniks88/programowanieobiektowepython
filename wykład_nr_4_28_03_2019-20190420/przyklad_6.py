class Date:
    _formatting = {'rmd' : '{d.year}-{d.month:02}-{d.day:02}',
                          'mdr' : '{d.month:02}/{d.day:02}/{d.year}',
                          'dmr' : '{d.day:02}/{d.month:02}/{d.year}'}
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day
 
    def __format__(self, fcode = ''):
        if fcode == '':
            fcode = 'rmd'
        representation = self._formatting[fcode]
        return representation.format(d = self)
            

date1 = Date(2009,6,5)
print(format(date1))
print(format(date1, 'dmr'))
print("Data: {:mdr}".format(date1))
print(f"Data: {date1:mdr}")




input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



