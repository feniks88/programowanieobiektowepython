# surowe bajty
s = 'dąbek'              # napis s ma 5 znaków Unicode
print(s)
print(len(s))           
b1 = s.encode('utf8')    # kodowanie str -> bytes przy użuciu utf-8 (b1 - sekwencja bajtów)
print(b1)                # bytes - typ niemutowalny!  
print(len(b1))           # bytes b1 ma 6 bajtów
                         # znak ź kodowany jest przez dwa bajty
b1.decode('utf8')        # dekodowanie bytes -> str (przy użuciu utf-8)

b2 = bytes(s, encoding = 'utf8')    # inny sposób tworzenia sekewcji bajtów
print(b2)

print(b2[1])             # każdy element jest liczbą z zakresu 0 - 255
print(b2[:1])            # wycinek sekwecji bytes jest tego samego typu

ba = bytearray(b2)
print(ba)
print(ba[-1:])
ba[0] = b'z'[0]          # bytearray - typ mutowalny!
print(ba.decode('utf8'))
#
# tworzenie obiektu bytes z surowych danych z tablicy
import array
numbers1 = array.array('h', [-3,-1,0,1,3])      #  'h' - szesnastobitowe liczby całkowite
b3 = bytes(numbers1)
print(b3)
numbers2 = array.array('h')
numbers2.frombytes(b3)
print(numbers2)


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



