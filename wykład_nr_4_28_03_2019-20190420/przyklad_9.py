import math, reprlib
from array import array

class Vektor:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, components):
        self._components  = array(self.typecode, components)
      
    def __iter__(self):
        return iter(self._components)
    
    def __repr__(self):
        components = reprlib.repr(self._components)
        components = components[components.find('['):-1] # usuwanie "array('d'," i końcowego nawiasu ")"
        return f'{type(self).__name__}({components})'

    def __str__(self):
        return str(tuple(self))  # bo wetkor jest iterowalny 

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(self._components))  

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.sqrt(sum(x ** 2 for x in self))  # może być więcej składników niż 2

    def __bool__(self):
        return bool(abs(self))  


    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)



v1 = Vektor([2,3,4,5,-3,4,7])
print('v1 ->',v1)
print('repr(v1) ->',repr(v1))
print('abs(v1) ->',abs(v1))



input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



