import math
from array import array

class Vektor2D:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, x, y):
        self._x = float(x)
        self._y = float(y)
        
    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y
      
    def __iter__(self):
        return (e for e in (self.x,self.y))
    
    def __repr__(self):
        return '{0}({1!r}, {2!r})'.format(type(self).__name__, *self)  

    def __str__(self):
        return str(tuple(self))  

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(array(self.typecode, self)))  

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.hypot(self.x, self.y)  

    def __bool__(self):
        return bool(abs(self))  

    def __hash__(self):
        return hash(self.x) ^ hash(self.y)

    def angle(self):
        return math.atan2(self.y, self.x)

    def __format__(self, fcode = ''):
        if fcode.endswith('p'):
            fcode = fcode[:-1]
            pc = (abs(self), self.kąt())
            representation = '<{}, {}>'
        else:
            pc = self
            representation = '({}, {})'
        components = (format(c, fcode) for c in pc)
        return representation.format(*components)
    

    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)


v1 = Vektor2D(2,3)
s = set()
print(s)
s.add(v1)
print(s)



input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



