#alternatywny konstruktor
import time

class Date:
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day
    @classmethod
    def today(cls):
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)

    def __repr__(self):
        return "{0}({1.year},{1.month},{1.day})".format(
            type(self).__name__, self)


date1 = Date(1999,4,11)
date2 = Date.today()
print(date1)
print(date2)


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



