import math
from array import array

class Vector2D:
    typecode = 'd'      # minimum ośmiobajtowa liczba zmiennoprzecinkowa
    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)
        
    def __iter__(self):
        return (e for e in (self.x,self.y))
    
    def __repr__(self):
        return '{0}({1!r}, {2!r})'.format(type(self).__name__, *self)  

    def __str__(self):
        return str(tuple(self))  

    def __bytes__(self):
        return (bytes([ord(self.typecode)]) +  
                bytes(array(self.typecode, self)))  

    def __eq__(self, other):
        return tuple(self) == tuple(other)  # bo wetkor jest iterowalny 

    def __abs__(self):
        return math.hypot(self.x, self.y)  

    def __bool__(self):
        return bool(abs(self))  

    def angle(self):
        return math.atan2(self.y, self.x)

    def __format__(self, fcode = ''):
        if fcode.endswith('p'):
            fcode = fcode[:-1]
            pc = (abs(self), self.angle())
            representation = '<{}, {}>'
        else:
            pc = self
            representation = '({}, {})'
        components = (format(c, fcode) for c in pc)
        return representation.format(*components)
    

    @classmethod  
    def frombytes(cls, sequence_of_bytes):  
        typecode = chr(sequence_of_bytes[0])  
        memv = memoryview(sequence_of_bytes[1:]).cast(typecode)  
        return cls(*memv)


v1 = Vector2D(2,3)
print('współrzędne biegunowe ->',format(v1,'p'))
print('współrzędne biegunowe ->',format(v1,'.3ep'))
print('współrzędne biegunowe ->',format(v1,'.5fp'))
print('współrzędne kartezjańskie ->',format(v1))


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



