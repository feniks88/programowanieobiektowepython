# wirtualna podklasa

import random
from lottery_machine import LotteryMachine

#Klasa WirtualLotto jest rejestrowana jako wirtualna podklasa
#klasy LotteryMachine.
#Klasa WirtualLotto rozszerza klasę list.

@LotteryMachine.register
class WirtualLotto(list):
    def pick(self):
        if self:
            index = random.randrange(len(self))
            return self.pop(index)
        else:
            raise LookupError
        
    #WirtualLotto.załaduj jest tym samym co list.extend 
    load = list.extend
    
    def loaded(self):
        return bool(self)

    def show_items(self):
        return tuple(sorted(self))

  

#W starszych wersjach Pythona (3.3 i wcześniej)
#nie możemy korzystać z register() jako dekoratora
#i musimy metodę register() wywołać standardowo.
# LotteryMachine.register(WirtualLotto)

wlotto = WirtualLotto(range(15))
print(issubclass(WirtualLotto, LotteryMachine))
print(isinstance(wlotto, LotteryMachine))
wlotto.load([15,16])
print(wlotto.pick())
print(wlotto.show_items())

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")
