# ręczne korzytanie z iteratora

# print("Pęta for iterująca po obiekcie typu str: ")
text = 'ABC'
# for ch in text:
#     print(ch)

# a jak to wygląda w praktyce
print("\nRęczne korzystanie z iteratora: ")
# Tworzenie iteratora
it = iter(text)  # Wywołuje s.__iter__()
# Uruchamianie iteratora
print(next(it))  #Wywołuje it.__next__()
print(next(it))
print(next(it))
# następuje zgłoszenie wyjątku StopIteration
print(next(it))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



