import collections.abc, pprint

Card = collections.namedtuple('Card', ['rank', 'suit'])

class Deck2(collections.abc.MutableSequence):
    RANKS = [str(n) for n in range(2,11)] + list('JQKA')
    SUITS = 'kier karo trefl pik'.split()
    def __init__(self):
        self._cards = [Card(suit,rank) for suit in
                       Deck2.RANKS for rank in Deck2.SUITS]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, index):
        return self._cards[index]

    def __setitem__(self,index,value):
        self._cards[index] = value

    def __delitem__(self,index):
        del self._karty[index]

    def insert(self,index,value):
        self._karty.insert(index,value)


deck = Deck2()
pprint.pprint(deck[:8])
print("-"*60)
from random import shuffle
shuffle(deck)
pprint.pprint(deck[:8])


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")

