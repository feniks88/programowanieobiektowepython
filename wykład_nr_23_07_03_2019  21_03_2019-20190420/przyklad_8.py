def print_classtree(cls, indent = 0):
    """Funkcja wyświetlająca 'drzewo' podklas klasy cls"""
    if indent == 0:
        print(cls.__name__)
    else:
        print('-' * indent + '>', cls.__name__)
    for subcls in cls.__subclasses__():
        print_classtree(subcls, indent + 3)

print_classtree(Exception)
