class A:
    pass

class B(A):
    pass


a = A()
b = B()

print('type(a) == A')
print(type(a) == A)
print('isinstance(a,A)')
print(isinstance(a,A))
print()
print('type(b) == B')
print(type(b) == B)
print('isinstance(b,B)')
print(isinstance(b,B))
print()
print('type(b) == A')
print(type(b) == A)
print('isinstance(b,A)')
print(isinstance(b,A))
print()
print('issubclass(A,B)')
print(issubclass(A,B))
print()
print('issubclass(B,A)')
print(issubclass(B,A))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")




