import re, reprlib
RE_WORD = re.compile('\w+')

class Sentence5:
    def __init__(self, text):
        self.text = text

    def __repr__(self):
        return '{type(self).__name__}({reprlib.repr(self.tekst)})'

    def __iter__(self):
        return (match.group() for match in RE_WORD.finditer(self.text))



# użycie zdefiniowanej klasy
sentence = Sentence5('Można oczy zamknąć na rzeczywistość, ale nie na wspomnienia.')
for word in sentence:
    print(word)

print('\nUżycie funkcji list() na obiekcie: ')	
print(list(sentence))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



