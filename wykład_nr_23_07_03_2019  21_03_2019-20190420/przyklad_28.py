# przykłady użycia funkcji generatora z biblioteki standardowej

import itertools
print(list(itertools.chain('ABCD',[11,12,13])))

print(list(itertools.chain.from_iterable(enumerate('ABCD'))))

print(list(zip('XYZ',range(6))))

print(list(zip('XYZ',range(6),(11,12,13,14))))

print(list(itertools.zip_longest('XYZ',range(5))))

print(list(itertools.zip_longest('XYZ',range(5),fillvalue = '*')))

print(list(itertools.product('xyz',range(2))))

suits = 'karo kier pik trefl'.split()
print(list(itertools.product('AK',suits)))

print(list(itertools.product('XYZ')))

print(list(itertools.product('XYZ',repeat = 2)))

print(list(itertools.product((0,1),repeat = 3)))

print()

rows = itertools.product('XY',(0,1),repeat = 2)
for r in rows:
    print(r)


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



