# klasa Deck spełenijąca protokuł sekwencji

import collections, pprint


Card = collections.namedtuple('Card', ['rank', 'suit'])

class Deck:
    RANKS = [str(n) for n in range(2,11)] + list('JQKA')
    SUITS = 'kier karo trefl pik'.split()
    def __init__(self):
        self._cards = [Card(suit,rank) for suit in
                       Deck.RANKS for rank in Deck.SUITS]
    def __len__(self):
        return len(self._cards)

    def __getitem__(self, index):
        return self._cards[index]

#małpie łatanie


deck = Deck()
pprint.pprint(deck[:4])
print()

def set_card(deck,index,card):
    deck._cards[index] = card


#dynamiczne uzupełnienie odpowiedniej metody	
Deck.__setitem__ = set_card

from random import shuffle
shuffle(deck)
pprint.pprint(deck[:4])





input("\n\nAby zakończyć program, naciśnij klawisz Enter.")




