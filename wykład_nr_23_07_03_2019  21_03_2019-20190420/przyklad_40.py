# podklasa II

import random, pprint
from lottery_machine import LotteryMachine

class Lotto(LotteryMachine):
    def __init__(self, iterable):
        self._balls = list(iterable)

    def load(self, iterable):
        self._balls.extend(iterable)

    # wybieranie elementu z losowej pozycji
    def pick(self):
        try:
            index = random.randrange(len(self._balls))
        except ValueError:
            raise LookupError
        return self._balls.pop(index)

# Przysłaniamy metody konkretne odziedziczone po abstrakcyjnej klasie bazowej.
# Implementujemy je w bardziej efektywny sposób.

    def loaded(self):
        return bool(self._balls)

    def show_items(self):
        return tuple(sorted(self._balls))


lotto = Lotto(range(1,50))
for i in range(6):
    print(lotto.pick())

pprint.pprint(lotto.show_items(), width = 20,compact = True)

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")
