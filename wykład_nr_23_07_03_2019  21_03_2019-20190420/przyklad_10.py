# zgłaszanie i przechwytywanie wyjątków

def division(numerator, denominator):
    if denominator == 0:
        raise ValueError('dzielnik jest zerem')
    else:
        return numerator/denominator

for i in range(-2,3):
    try:
        print(division(1,i))
    except ValueError as exep:
        print(f'przechwycony wyjątek dla {i} - {exep}')
		

