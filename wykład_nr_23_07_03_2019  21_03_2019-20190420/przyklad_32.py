# klasa Deck spełenijąca protokuł sekwencji

import collections

# Klasa Card jako krotka nazwana
Card = collections.namedtuple('Card', ['rank', 'suit'])

c = Card('A','kier')
print(c)
print(c[0])
print(c[1])
print()
print(c.rank)
print(c.suit)

# definicja klasy
class Deck:
    RANKS = [str(n) for n in range(2,11)] + list('JQKA')
    SUITS = 'kier karo trefl pik'.split()
    def __init__(self):
        self._cards = [Card(rank,suit) for rank in
                       Deck.RANKS for suit in Deck.SUITS]
    def __len__(self):
        return len(self._cards)

    def __getitem__(self, index):
        return self._cards[index]

print('\nKlasa talia: ')
deck = Deck()
print(len(deck))
print(deck[0],deck[2],deck[6])

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")




