class MyException(Exception): pass

try:
   raise MyException("informacje", "nazwa_pliku", 4)
except MyException as exep:
   print("Zdarzenie: problem {0[2]} z plikiem {0[1]}: {0[0]}". format(
      exep.args))


