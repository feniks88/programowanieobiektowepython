# podklasa I
import random
from lottery_machine import LotteryMachine


#Klasa Bingo rozszerza klasę LotteryMachine.

class Bingo(LotteryMachine):
    def __init__(self, items):
        self._items = []
        self.load(items)

    def load(self, items):
        self._items.extend(items)
        random.shuffle(self._items)

    def pick(self):
        try:
            return self._items.pop()
        except IndexError:
            raise LookupError

    # nasz obiket może być wywoływalny jak funkcja
    def __call__(self):
        return self.pick()

bingo = Bingo([1,2,3,4,5,6,7,8,9,10])
print(bingo.pick())
bingo.load(range(11,16))
print(bingo())
print(bingo.show_items())

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")
