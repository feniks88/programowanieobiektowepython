# ilustracja komunikacja między obiektami

class Player:
    """Gracza w grze strzelance."""
    def __init__(self, life=10):
        self.life = life

    def hit(self, enemy, power_of_hit = 5):
        print("Gracz zadaje obcemu cios.")
        enemy.damage(power_of_hit)  # realizacja komunikacji między obiektami


class Enemy:
    """Obcy w grze strzelance."""
    def __init__(self, life = 5):
        self.life = life

    def damage(self, power_of_damage):
        self.life -= power_of_damage
        if self.life <= 0:
            print("Obcy z trudem łapie oddech. To już koniec!")
            print("Obcy umiera!")
        else:
            print("Obcy porządnie oberwał ale, trzyma się na nogach.")


# konkretyzacja obiektów
player = Player()
enemy  = Enemy()

player.hit(enemy)




