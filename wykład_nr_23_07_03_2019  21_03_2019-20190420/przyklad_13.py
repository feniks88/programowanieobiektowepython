# przykład klasy, której instancje są obiektami iterowalnymi

import re, reprlib
RE_SŁOWO = re.compile('\w+')

class Sentence1:
    def __init__(self, text):
        self.text = text
        self.words = RE_SŁOWO.findall(text)

    def __getitem__(self, index):
        return self.words[index]

    def __len__(self):
        return len(self.words)

    def __repr__(self):
        return f'Sentence1({reprlib.repr(self.text)})'

# użycie zdefiniowanej klasy

sentence = Sentence1('Można oczy zamknąć na rzeczywistość, ale nie na wspomnienia.')

print(sentence)

print('\nUżywamy pętli...\n')
for word in sentence:
	print(word)


print("\nUżywamy funkcji 'list()'...\n")
print(list(sentence))
print(sentence[0], sentence[5], sentence[-1])


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



