import re, reprlib
RE_WORD = re.compile('\w+')

class Sentence2:
    def __init__(self, text):
        self.text = text
        self.words = RE_WORD.findall(text)

    def __repr__(self):
        return f'{type(self).__name__}({reprlib.repr(self.text)})'

    def __iter__(self):
        return Iterator(self.words)

# klasa iterator, raczej nie piszemy klas itereatorów!
class Iterator:
    def __init__(self, words):
        self.words = words
        self.index = 0

    def __next__(self):
        try:
            word = self.words[self.index]
        except IndexError:
            raise StopIteration()       # wzbudzenie wyjątku StopIteration
        self.index += 1
        return word
    def __iter__(self):                 # standardowo metoda __iter__ powinna zwracać instancje iteratora
        return self
            

# użycie zdefiniowanej klasy
sentence = Sentence2('Można oczy zamknąć na rzeczywistość, ale nie na wspomnienia.')
for word in sentence:
    print(word)

print('\nUżycie funkcji list() na obiekcie: ')	
print(list(sentence))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



