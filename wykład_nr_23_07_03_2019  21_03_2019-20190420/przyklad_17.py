import re, reprlib
RE_WORD = re.compile('\w+')

class Sentence3:
    def __init__(self, text):
        self.text = text
        self.words = RE_WORD.findall(text)

    def __repr__(self):
        return '{type(self).__name__}({reprlib.repr(self.text)})'

    def __iter__(self):
        for word in self.words:
            yield word
        return

# użycie zdefiniowanej klasy
sentence = Sentence3('Można oczy zamknąć na rzeczywistość, ale nie na wspomnienia.')
for word in sentence:
    print(word)

print('\nUżycie funkcji list() na obiekcie: ')	
print(list(sentence))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



