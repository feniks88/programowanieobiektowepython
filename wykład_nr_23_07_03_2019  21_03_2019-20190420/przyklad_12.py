# przykład ilustrujący użycie własnych wyjątków

def found_target1(table, target):
    found = False
    for row, record in enumerate(table):
        for column, field in enumerate(record):
            for index, item in enumerate(field):
                if item == target:
                    found = True
                    break
            if found:
                break
        if found:
            break
    if found:
        print(f"wartość znaleziona w ({row}, {column}, {index})")
    else:
        print("wartość nieznaleziona")



class FoundException(Exception): pass	  # zdefiniowanie wyjątku

def found_target2(table, target):
    try:   # blok przechwytywania wyjątku
        for row, record in enumerate(table):
            for column, field in enumerate(record):
                for index, item in enumerate(field):
                    if item == target:
                        raise FoundException() # zgłoszenie wyjątku!
    except FoundException:		
        print(f"wartość znaleziona w ({row}, {column}, {index})")
    else:
        print("wartość nieznaleziona")

characters = [ ["#&$@", "<>[]", "(){}"],  ["+=-*", ",.;:", "?!\/"] ]
found_target1(characters, '!')
found_target2(characters, '?')


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



