# wyrażenie generatora

def gen_AB():
    print('START')
    yield 'A'
    print('DALEJ')
    yield 'B'
    print('KONIEC')

print('\nlista składana:')	
x1 = [i * 3 for i in gen_AB()]

for k in x1:
    print('-->', k)

print(x1)


print('\nwyrażenie generatora:')
x2 = (i * 3 for i in gen_AB())
print(x2)
for k in x2:
    print('-->', k)



input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



