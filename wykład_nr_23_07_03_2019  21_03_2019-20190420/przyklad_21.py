import re, reprlib
RE_WORD = re.compile('\w+')

class Sentence4:
    def __init__(self, text):
        self.text = text

    def __repr__(self):
        return '{type(self).__name__}({reprlib.repr(self.tekst)})'

    def __iter__(self):
# Funkcja re.finditer() jest leniwą wersją re.findall(),
# która nie zawraca listy ale generator produkujący wystąpienia
# re.MatchObject na żądanie.

# finditer buduje iterator z dopasowaniami wyrażenia
#regularnego RE_WORD na bazie Self.tekst produkując wystąpienia MatchObject.

        for match in RE_WORD.finditer(self.text):
            yield match.group()   # match.group()
                                  # wyciąga faktycznie dopasowany
                                  # tekst z wystąpienia MatchObject


# użycie zdefiniowanej klasy
sentence = Sentence4('Można oczy zamknąć na rzeczywistość, ale nie na wspomnienia.')
for word in sentence:
    print(word)

print('\nUżycie funkcji list() na obiekcie: ')	
print(list(sentence))


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



