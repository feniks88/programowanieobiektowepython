# klasa Deck spełenijąca protokuł sekwencji

import collections


Card = collections.namedtuple('Card', ['rank', 'suit'])

class Deck:
    RANKS = [str(n) for n in range(2,11)] + list('JQKA')
    SUITS = 'kier karo trefl pik'.split()
    def __init__(self):
        self._cards = [Card(suit,rank) for suit in
                       Deck.RANKS for rank in Deck.SUITS]
    def __len__(self):
        return len(self._cards)

    def __getitem__(self, index):
        return self._cards[index]

#naszej talii nie można tasować, ale czy pisać specjalną metodę?
#skoro klasa Deck działa jak sekwencja wykorzystajmy metodę
#suffle() modułu random do „wymieszania sekwencji na miejscu”.

deck = Deck()
from random import shuffle
shuffle(deck)


#klasa Talia implementuje jedynie protokół niezmiennej sekwencji
#sekwencje zmienne muszą również zapewniać metodę __setitem__()


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")




