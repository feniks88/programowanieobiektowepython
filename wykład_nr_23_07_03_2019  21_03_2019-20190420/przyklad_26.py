# funkcja generatora produkująca wartości z innych obiektów iterowalnych
# w szczególności innych iteratorów
def chain(*iterables):
    for it in iterables:
        for i in it:
            yield i


S = 'abcd'
L = (1,2,3,4,5,6)
print(list(chain(S,L)))

# użycie składni yield from
def chain(*iterables):
    for it in iterables:
        yield from it

S = 'abcd'
L = (1,2,3,4,5,6)
print(list(chain(S,L)))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")
