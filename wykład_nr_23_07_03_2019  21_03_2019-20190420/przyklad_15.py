# emulowanie działania pętli for przy pomocy pętli while:

text = 'ABC'    # obiekt str jest sekwencją więc jest obiektem iterowalnym
it = iter(text) # funkcja iter() zwraca iterator it
while True:
    try:
        print(next(it))     # funkaca next() wzbudza wyjątek StopIteration,
    except StopIteration:   # gdy nie ma już elementów
        del it
        break


input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



