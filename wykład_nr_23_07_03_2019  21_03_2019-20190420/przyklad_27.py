# przykłady użycia funkcji generatora z biblioteki standardowej

import itertools

print(list(itertools.compress('wretwASdffe', (1,0,1,1,0,0,1,1))))

def if_vovel(letter):
    return letter.lower() in 'aąeęiouóy'

print(list(itertools.dropwhile(if_vovel, 'Aefgue')))

print(list(filter(if_vovel, 'Aefgue')))

print(list(filter(None, (1,3,0,4,5))))

print(list(itertools.filterfalse(if_vovel, 'AsfdeeFgiH')))

print(list(itertools.takewhile(if_vovel, 'AeeiFgiH')))

print(list(itertools.islice('IepDjsda',4)))

print(list(itertools.islice('IepDjsda',4,7)))

print(list(itertools.islice('IepDjsda',1,7,2)))

dane = [4,5,7,8,9,2,7,1,0,12,4]

# suma krocząca
print(list(itertools.accumulate(dane)))
# minimum kroczące
print(list(itertools.accumulate(dane,min)))
# maksimum kroczące
print(list(itertools.accumulate(dane,max)))

import operator
# iloczyn kroczący
print(list(itertools.accumulate(dane,operator.mul)))
# wartości silni od 1! do 10!
print(list(itertools.accumulate(range(1,11),operator.mul)))

print(list(enumerate('ABCD',1)))

print(list(map(operator.mul, range(8),[2,4,6])))

# tak działa funkcja zip()
print(list(map(lambda a, b: (a,b), range(8),[2,4,6])))
# srednia krocząca
nowe_dane = [4,5,7,8,9,2,7,1,0,12,4]
print(list(itertools.starmap(lambda a, b: b/a,
                           enumerate(itertools.accumulate(nowe_dane),1))))




input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



