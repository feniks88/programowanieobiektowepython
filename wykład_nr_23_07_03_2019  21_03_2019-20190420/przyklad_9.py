# Nieprawidłowe korzystanie z hierarchiczności wyjątków

#--------------------------------------------------
print('PRZYKŁAD 1:\n')
d1 ={0:'zero',1:'jeden',2:'dwa',3:'trzy'}

try:
    x = d1[4]
except LookupError:	# ZŁA KOLEJNOŚĆ!
    print("Wystąpił błąd wyszukiwania!")
except KeyError:
    print("Użyto nieprawidłowego klucza!")


#--------------------------------------------------
print('\n\n\nPRZYKŁAD 2:\n')
d2 = {1:'JEDEN',  2:'DWA',  4/3:'CZTERY TRZECIE'}

for i in range(6):
    try:
        n = 4/i
        print(d2[n])
    except Exception:	# ZŁA PRAKTYKA!
        print("Coś poszło nie tak!")




