# Przykłady – ciąg arytmetyczny (dwie implementacje)
# klasa

class Arithmetic_progression:
    def __init__(self, start, step, end = None):
        self.start = start
        self.step = step
        self.end = end
    def __iter__(self):
        # produkcja wartości wynik z uwzględnieniem typu kolejnych liczb
        result = type(self.start + self.step)(self.start)
        infinity = self.end is None
        index = 0    # stosujemy zmienną indeks aby ograniczyć skumulowany
                     # wpływ błędów zaokrągleń przy
                     # pracy z wartościami zmiennoprzecinkowymi.
        while infinity or result < self.end:
            yield result  
            index += 1
            result = self.start + self.step * index

# funkcja
def arithmetic_progression(start, step, end = None):
    result = type(start + step)(start)
    infinity = end is None
    index = 0
    while infinity or result < end:
        yield result
        index += 1
        result = start + step * index


# konkretyzacja obiektów klasy Arithmetic_progression i jego użycie
art1 = Arithmetic_progression(0,1,5)
print(list(art1))
art2 = Arithmetic_progression(1,3/7,3)
print(list(art2))

from fractions import Fraction
from decimal import Decimal
art3 = Arithmetic_progression(1,Fraction(3,7),3)
print(list(art3))
art4 = Arithmetic_progression(1,Decimal('0.1'),1.5)
print(list(art4))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



