# przykłady użycia funkcji redukujących obiekty iterowalne

print(all([3,4,5]))

print(all([3,0,5]))

print(all([]))

print(any([1,0,3]))

print(any([0.0,0,0]))

print(any([]))

g = (i for i in [0,(),7,8,9])
print(any(g))
print(next(g))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



