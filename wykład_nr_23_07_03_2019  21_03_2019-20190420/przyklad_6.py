# definicja funkcji
def inverse(x):
    """Funkcja wyznaczająca odwrtotności liczby"""
    try:
	    i = 1/x
    except:
	    print(f"Przechwycenie wyjątku dla {x}")
    else:
	    print(f"Odwrotnością {x} jest {i}")

# wywołanie funkcji		
inverse(2)
inverse(0)

