import random

class Card:
    """Karta do gry"""
    RANKS = ["A", "2", "3", "4", "5", "6", "7",
              "8", "9", "10", "J", "Q", "K"]
    SUITS = ["karo", "kier", "trefl", "pik"]

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __str__(self):
        text = self.rank + "_" + self.suit
        return text


class Hand:
    """Ręka - karty do gry znajdujące się w ręku gracza"""

    def __init__(self):
        self.cards = []  # tu będą obiekty klasy karta

    def __str__(self):
        if self.cards:
            text = ""
            for card in self.cards:
                text += str(card) + "  "
        else:
            text = "pusta"
        return text

    def clear_hand(self):
        self.cards = []

    def add_card(self, card):
        self.cards.append(card)

    def give_card(self, card, other_hand):
        self.cards.remove(card)
        other_hand.add_card(card)

# klasa Talia oparta o kalsę Hand
class Deck(Hand):
    """Talia kart do gry"""
    def create_deck(self):
        for rank in Card.RANKS:
            for suit in Card.SUITS:
                self.add_card(Card(rank,suit))

    def shuffle_deck(self):
        import random
        random.shuffle(self.cards)

    def handing_out_cards(self, hands, number_of_cards = 1):
        for i in range(number_of_cards):
            for hand in hands:
                if self.cards:
                    top_card = self.cards[0]
                    self.give_card(top_card, hand)
                else:
                    print("Koniec kart w talii!")

# Część główna programu:
deck = Deck()
print("Została utworzona nowa talia:")
print(deck)

deck.create_deck()
print("\nW talii są 52 karty: ")
print(deck)

deck.shuffle_deck()
print("\nPotasowana talia kart: ")
print(deck)

my_hand = Hand()
your_hand = Hand()
hands = [my_hand, your_hand]
deck.handing_out_cards(hands, 5)

print("\nKarty w mojej ręce: ")
print(my_hand)
print("\nKarty w Twojej ręce: ")
print(your_hand)
print("\nPozostałe karty w talii: ")
print(deck)
