# przykłady użycia funkcji generatora z biblioteki standardowej

import itertools, operator

sequence = itertools.count()
print(next(sequence),next(sequence),next(sequence))

print(list(itertools.islice(itertools.count(2,0.3),4)))

around = itertools.cycle('XYZ')
print(next(around))

print(list(itertools.islice(around,8)))

rp = itertools.repeat(7)
print(next(rp),next(rp),next(rp))

print(list(itertools.repeat('A',4)))

print(list(map(operator.mul, range(6), itertools.repeat(5))))


print(list(itertools.combinations('ABC',2)))

print(list(itertools.combinations_with_replacement('ABC',2)))

print(list(itertools.permutations('ABC',2)))

print(list(itertools.permutations('ABC')))

print(list(itertools.combinations('ABC', 3)))

print(list(itertools.groupby('aaaabbbcc')))

for ch, group in itertools.groupby('aaaabbbcc'):
    print(ch,'-->',list(group))

print()

trees = ['klon', 'dąb', 'sosna', 'jodła', 'brzoza', 'topola', 'świerk', 'buk', 'modrzew', 'kosodrzewina']
trees.sort(key = len)
for length, group in itertools.groupby(reversed(trees),len):
    print(length,'-->',list(group))

print()


print(list(itertools.tee('XYZ')))
g1,g2 = itertools.tee('XYZ')
print(next(g1))
print(next(g2))
print(next(g2))
print(list(g1))
print(list(g2))
print(list(zip(*itertools.tee('XYZ'))))

input("\n\nAby zakończyć program, naciśnij klawisz Enter.")



