class Card:
    """Karta do gry"""
    RANKS = ["A", "2", "3", "4", "5", "6", "7",
              "8", "9", "10", "J", "Q", "K"]
    SUITS = ["karo", "kier", "trefl", "pik"]

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __str__(self):
        text = self.rank + "_" + self.suit
        return text


class Hand:
    """Ręka - karty do gry znajdujące się w ręku gracza"""

    def __init__(self):
        self.cards = []  # tu będą obiekty klasy karta

    def __str__(self):
        if self.cards:
            text = ""
            for card in self.cards:
                text += str(card) + "  "
        else:
            text = "pusta"
        return text

    def clear_hand(self):
        self.cards = []

    def add_card(self, card):
        self.cards.append(card)

    def give_card(self, card, other_hand):
        self.cards.remove(card)
        other_hand.add_card(card)


# Część głowna programu:
card_1 = Card(rank="A", suit="trefl")
print("Wyświetlenie obiektu klasy Karta:")
print(card_1)

card_2 = Card(rank="K", suit="pik")
card_3 = Card(rank="2", suit="trefl")
card_4 = Card(rank="10", suit="karo")
card_5 = Card(rank="5", suit="kier")

print("\nWyświetlenie kolejnych obiektów klasy Karta:")
print(card_2)
print(card_3)
print(card_4)
print(card_5)

my_hand = Hand()
print("\nWyświetlenie zawartości mojej ręki (bez kart): ")
print(my_hand)

my_hand.add_card(card_1)
my_hand.add_card(card_2)
my_hand.add_card(card_3)
my_hand.add_card(card_4)
my_hand.add_card(card_5)

print("\nWyświetlenie zawrtości mojej ręki po dodaniu kart: ")
print(my_hand)
