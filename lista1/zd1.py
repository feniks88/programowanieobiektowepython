import random

def get_number(a, b, text):
    """Pobiera i zwraca liczbe calkowita z zakresu od a do b"""
    while True:
        try:
            data = input(f"{text} (liczba z zakresu {a} do {b}): ")  # obsluga wyjatkow
            number = int(data)
        except ValueError:
            print(f"{data} - to nie jest liczba")
        else:
            if a <= number <= b:
                return number
            else:
                print("przekroczono zakres")


def lay_mines(number_of_mines, rows, columns):

    #todo listy, zbiory, słowniki, krotki

    """Zwraca zbior przechowujacy wspolrzedne min"""
    mines=set()
    while len(mines) < number_of_mines:
        m = random.randrange(rows)
        n = random.randrange(columns)
        mines.add((m,n))
    return mines


def number_of_neighboring_mines(field, mines, rows, columns):

    """Zwraca wartosc bedaca liczba pol z minami dla pola field"""

    i = field[0]
    j = field[1]
    counter = 0
    list_of_fields = [(i-1, j-1), (i-1, j), (i-1,j+1), 
                      (i,j-1), (i,j+1),
                      (i+1,j-1), (i+1,j), (i+1,j+1)]
    
    for m,n in list_of_fields:
        if 0 <= m < rows and 0 <= n < columns and (m,n) in mines:
            counter += 1
            
    return counter


def creat_board(mines,rows,columns, mine = '*'):
    """Tworzy plansze do gry w sapera"""
    board = []
    for i in range(rows):
        line = []
        for j in range(columns):
            if (i,j) in mines:
                line.append(mine)
            else:
                line.append(number_of_neighboring_mines((i,j), mines, rows, columns))
        board.append(line)
    return board

def reveal_fields(field, board, printable_field, rows, columns):
    """Opis"""
    i = field[0]
    j = field[1]

    if not (0 <= i < rows and 0 <= j < columns) or (i,j) in printable_field:
        return
    printable_field.add((i,j))

    if board[i][j] != 0:
        return
    for m, n in [(i-1, j-1), (i-1, j), (i-1,j+1),
                      (i,j-1), (i,j+1),
                      (i+1,j-1), (i+1,j), (i+1,j+1)]:
        reveal_fields((m,n), board,printable_field,rows,columns)



mines = lay_mines(10,10,10)
board = creat_board(mines,10,10)

for line in board:
    for e1 in line:
        print(f"{e1:^3}", end = ' ') #wysrodkowany na 3 polach, bez nowej linii
    print()






            







