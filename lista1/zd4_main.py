import zd4
import pickle


phone1 = zd4.Smartphone("Samsung", "S9+", "3000")
print(phone1)

phone2 = zd4.Smartphone("Samsung", "A5", "500")
print(phone2)

phone3 = zd4.Smartphone("Samsung", "S7", "1500")
print(phone3)

file = open("phones.dat", "wb")
pickle.dump(phone1,file)

pickle.dump(phone2,file)

pickle.dump(phone3,file)

file.close()

file = open("phones.dat", "rb")
print(pickle.load(file))
print(pickle.load(file))
print(pickle.load(file))

file.close()