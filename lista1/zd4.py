class Smartphone:
    def __init__(self,manufacturer, model, price):
        self.__manufacturer = manufacturer
        self.__model = model
        self.__price = price

    @property
    def price(self):
        return self.__price

    @property
    def manufacturer(self):
        return self.__manufacturer

    @property
    def model(self):
        return self.__model

    @price.setter
    def price(self, value):
        self.__price = value

    @manufacturer.setter
    def manufacturer(self, value):
        self.__manufacturer = value

    @model.setter
    def model(self, value):
        self.__model = value

    def __str__(self):
        return f"Twój smartwon to {self.__manufacturer} {self.__model} o cenie {self.__price}"

