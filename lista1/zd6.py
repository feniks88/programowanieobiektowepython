class TV:
    def __init__(self, channel, volume):
        self.__channel = channel
        self.__volume = volume

    @property
    def channel(self):
        return self.__channel

    @property
    def volume(self):
        return self.__volume

    @channel.setter
    def channel(self, value):
        try:
            number = int(value)
        except ValueError:
            print(f"{value} - to nie jest liczba")
        else:
            if 0 <= number <= 1000:
                self.__channel = number
            else:
                print("Przekroczono zakres")

    @volume.setter
    def volume(self,value):
        if( 0 < self.__volume + value  > 100):
            print(f"Glosnosc: {self.__volume} ")
        else:
            self.__volume += value

    def __str__(self):
        return f"Jesteś na kanale {self.channel} z glosnościa {self.volume}"

    def __del__(self):
        print("TV off")

def Menu_glowne():
    return """Menu:
    [1] Wybierz kanal
    [2] Zmniejsz glosnosc
    [3] Zwieksz glosnosc
    [4] Wylacz"""

on = TV(1,10)

print("Telewizor włączony")
take = True
while take is True:
    print(on)
    try:
        data = input(Menu_glowne())  # obsluga wyjatkow
        number = int(data)
    except ValueError:
        print(f"{data} - nie ma takiej opcji")
    else:
        if 1 <= number <= 4:
            if number is 1:
                try:
                    data = input("Podaj numer kanalu(1-999): ")
                    value = int(data)
                except ValueError:
                    print(f"{data} - podales niepoprawna wartosc")
                else:
                    on.channel = value
            elif number is 2:
                on.volume = -1
            elif number is 3:
                on.volume = 1
            elif number is 4:
                take = False
                del on
        else:
            print("przekroczono zakres")