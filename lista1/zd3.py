class Account:
    def __init__(self, balance):
        self._balance = balance

    def pay(self, add):
        self._balance += add

    def take(self, minus):

        if(self._balance is 0):
            print("Brak srodkow")
        elif(minus > self._balance):
            print("Niewystarczajaco srodkow")
        else:
            self._balance -=  minus

    def __str__(self):
        return f"Stan konta to: {self._balance}"

    def show_balance(self):
        return self._balance


acc1 = Account(1350)

print(acc1)

print(acc1.show_balance())

acc1.pay(10000)
print(acc1)
acc1.take(20000)

acc1.take(15.50)
print(acc1)