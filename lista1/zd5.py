class RocketEngine:
    count = 0
    all_power = 0

    def __init__(self, name, power, working = False):
        self._name = name
        self._power = power
        self._working = working
        RocketEngine.count += 1
    def start(self):
        if not self._working:
            RocketEngine.all_power += self._power
            self._working = True
    def stop(self):
        if self._working:
            RocketEngine.all_power += self._power
            self._working = False

    def __str__(self):
        return f"Silnik {self._name}  Moc = {self._power} Pracuje = {self._working}"

    def __del__(self):
        RocketEngine.count -= 1

    @staticmethod
    def status():
        print(f"Ilosc: {RocketEngine.count} Aktywna moc: {RocketEngine.all_power}")

def eng_status(list_engines):
    RocketEngine.status()
    for engine in list_engines:
        print(engine)

if __name__='__main__':
    list_engines = [ ]
    mnvr_eng1 = RocketEngine("Manewrowy 1", 50)
    mnvr_eng2 = RocketEngine("Manewrowy 2", 50)
    list_engines.append(mnvr_eng1)
    list_engines.append(mnvr_eng2)
    print("Manewrowanie")
    mnvr_eng1.start()
    mnvr_eng2.start()
    eng_status(list_engines)
    print("Rozpedzenie do hiperpredkosci")
    warmup_eng1 = RocketEngine("Rozpedzacz 1", 500)
    warmup_eng2 = RocketEngine("Rozpedzacz 2", 500)
    list_engines.append(warmup_eng1)
    list_engines.append(warmup_eng2)
    warmup_eng1.start()
    warmup_eng2.start()
    eng_status(list_engines)
    print("Hiperpredkosci")
    hyper_eng1 = RocketEngine("Hiper 1", 40000)
    hyper_eng2 = RocketEngine("HIper 2", 40000)
    list_engines.append(hyper_eng1)
    list_engines.append(hyper_eng2)
    hyper_eng1.start()
    hyper_eng2.start()
    eng_status(list_engines)
    print("Wyjscie z hiperpredkosci")
    hyper_eng1.stop()
    hyper_eng2.stop()
    warmup_eng1.stop()
    warmup_eng2.stop()
    list_engines.remove(hyper_eng1)
    list_engines.remove(hyper_eng2)
    list_engines.remove(warmup_eng1)
    list_engines.remove(warmup_eng2)
    del hyper_eng2
    del hyper_eng1
    del warmup_eng1
    del warmup_eng2
    eng_status(list_engines)
    print("Cumowanie w porcie")

    mnvr_eng1.stop()
    mnvr_eng2.stop()
    list_engines.remove(mnvr_eng1)
    list_engines.remove(mnvr_eng2)
    del mnvr_eng1
    del mnvr_eng2

    eng_status(list_engines)