import random

class Coin:

    def __init__(self):
        self.throw()

    def throw(self):
        self.side = random.randint(0,1)

    def show_side(self):
        return self.side