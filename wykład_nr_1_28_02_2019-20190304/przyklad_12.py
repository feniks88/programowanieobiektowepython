# definicja klasy
class Pet:
    "Klasa reprezentująca domowego zwierzaka"
    def __init__(self, name):  # konstruktor
        self.name = name
        print("Urodził się nowy zwierzak!")
    def talk(self):
        print(f"Cześć. Jestem {self.name}!")



# część główna programu# definicja klasy
# class Pet:
#     """Klasa reprezentująca domowego zwierzaka"""
#     def __init__(self):  # konstruktor
#         self.name = "Bobek"
#         print("Urodził się nowy zwierzak!")
#
#
#
# # część główna programu
# my_pet1 = Pet()  # konkretyzacja obiektu
# my_pet2 = Pet()  # konkretyzacja obiektu
# print(my_pet1.name)
# print(my_pet2.name)
# print(my_pet1 is my_pet2)
my_pet1 = Pet("Topik")  # konkretyzacja obiektu
my_pet1.talk()          # wywołanie metody instancyjnej
my_pet2 = Pet("Topcia") # konkretyzacja obiektu
my_pet2.talk()          # wywołanie metody instancyjnej
# bezpośredni dostęp do atrybutu name obiektu my_pet1 i mu_pet2
print(my_pet1.name)
print(my_pet2.name)
print(my_pet2)
