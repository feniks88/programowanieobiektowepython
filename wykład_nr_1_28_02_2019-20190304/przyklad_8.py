# kacze typowanie

print(len("Ala ma kota"))           # argument jest ciągiem tekstowym
print(len((1,2,3,4,5)))             # argument jest krotką
print(len(["a","b", "c"]))          # argument jest listą
print(len({1,2,3,4}))               # argument jest zbiorem
print(len({"one":1, "two":2}))      # argument jest słownikiem