# właściwości - kontrola dostępu do atrybutów

import datetime
class Person:
    """Klasa reprezentująca osobę"""
    def __init__(self, name, birthday, married = False):
        self._name = name
        self._birthday = birthday
        self._married = married      # kontrola już na etapia tworzenia obiektu

    @property
    def married(self):
        return self._married

    @married.setter # ZMIANA WARTOSCI
    def married(self,value):
        if not isinstance(value, bool):
            print("Błędna wartość!")
        else:
            self._married = value

    # @married.deleter
    # def married(self):
    #     print("Nie można usunąć atrybutu!")


    @property
    def age(self):
        today = datetime.date.today()
        return today.year - self._birthday.year

    @property
    def name(self):
        return self._name

john = Person("John", datetime.date(1989,10,3))
print(john.name)
print(john.age)
john.married = 6
print(john.married)

# del john.married
