# klasa
class Pet:
    """Klasa reprezentująca domowego zwierzaka"""   # dokumentujący ciąg tekstowy
    pass


# konkretyzacja obiektu klasy Pet
my_pet = Pet()
print(type(my_pet))

# dodanie atrybutu do instancji klasy Pet
my_pet.name = "Bobek"
print(my_pet.name)



