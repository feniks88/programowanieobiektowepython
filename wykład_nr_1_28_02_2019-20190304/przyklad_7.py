import weakref

x = {1,2,3}
y = x

def f():
    print("Żegnaj...")


end = weakref.finalize(x, f)
print(end.alive)
del x       # usuwamy referencję do obiektu {1,2,3}
print(end.alive)
y = 'Ala'   # usuwamy referencję do obiektu {1,2,3}
print(end.alive)