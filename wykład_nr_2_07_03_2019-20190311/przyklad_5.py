values = [-3,0,3]

for i in range(4): # o jeden więcej niż len(wartości)
    try:
        r = 1 / values[i]
        print(f"Odwrotność {values[i]} to {r}")
    except IndexError:
        print(f"Indeks {i} przekracza długość listy.")
    except ZeroDivisionError:
        print(f"Nie mogę obliczyć odwrotności: {values[i]}")



