# program ilustrujący użycie instrukcji try ... finally
import string, collections

def frequency(element):
    return element[1]

words = collections.defaultdict(int)
characters = string.whitespace + string.punctuation + string.digits


try:
    file = None
    name_of_file = input("Podaj nazwę pliku: ")
    file = open(name_of_file, 'r', encoding="utf8")
    for line in file:
        for word in line.lower().split():
            word = word.strip(characters)
            if len(word) > 2:
                words[word] += 1
except FileNotFoundError as exep:
    print(exep)
else:
    for word, count in sorted(words.items(), key=frequency, reverse=True):
        print(f"'{word:<18}' - występuje {count} razy")
finally:
    if file:
        file.close()
        print("Plik został zamknięty!")
    else:
        print("Plik nie został w ogóle otwarty!")




input("\n\nAby zakończyć program, naciśnij klawisz Enter.")
