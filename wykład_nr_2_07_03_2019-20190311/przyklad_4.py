class Card:
    """Karta do gry"""
    RANKS = ["A", "2", "3", "4", "5", "6", "7",
              "8", "9", "10", "J", "Q", "K"]
    SUITS = ["karo", "kier", "trefl", "pik"]

    def __init__(self, rank, suit):
        self.rank = rank
        self.suit = suit

    def __str__(self):
        text = self.rank + "_" + self.suit
        return text


class Unprintable_Card(Card):
    """Karta, której figura i kolor nie jest ujawniany podczas jej wyświetlania"""
    def __str__(self):
        return "zakryta"


class Positionable_Card(Card):
    """Karta, która może być odkryta lub zakryta"""

    def __init__(self, rank, suit, face_up = True):
        super().__init__(rank, suit)
        self.is_face_up = face_up

    def __str__(self):
        if self.is_face_up:
            text = super(Positionable_Card, self).__str__()
        else:
            text = "X_xxxx"
        return text

    def flip(self):
        self.is_face_up = not self.is_face_up


# Część główna programu:
card_1 = Card("Q", "pik")
card_2 = Unprintable_Card("Q", "trefl")
card_3 = Positionable_Card("Q", "karo")

print("Wyświetlenie obiektu klasy Card:")
print(card_1)
print("\nWyświetlenie obiektu klasy Unprintable_Card:")
print(card_2)
print("\nWyświetlenie obiektu klasy Positionable_Card:")
print(card_3)

print("\nOdwrócenie stanu klasy Positionable_Card")
card_3.flip()

print("\nWyświetlenie obiektu klasy Positionable_Card:")
print(card_3)
