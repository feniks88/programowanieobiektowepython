import datetime

class Note:
    id = 0
    def __init__(self, text, tag):
        self.text = text
        self.tag = tag
        self.date = datetime.date.today()
        Note.id += 1
        self.id = Note.id
    def match(self, value):
        if self.text.find(value) or self.tag.find(value):
            return True
        else:
            return False
    def __str__(self):
        return f"{self.text} i {self.tag} {self.id}"


class Notebook:
    def __init__(self):
        self.notes = []
    def new_note(self, value):
        self.notes.append(value)
    def modify_text(self,ID, value):
        if len(self.notes) > ID or ID < 0:
            return "error"
        else:
            self.notes[ID] = value
    def modify_tag(self,ID, value):
        if len(self.notes) > ID or ID < 0:
            return "error"
        else:
            self.notes[ID] = value
    def search(self, value):
        match_notes = []
        for i in range (0, len(self.notes)):
            if self.notes[i].find(value):
                match_notes.append(self.notes[i])
        return match_notes
    def __str__(self):
        output = ""
        for i in range(0, len(self.notes)):
            output += f"{self.notes[i]} \n"
        return output
    
note1 = Note ("kwiate", "kurwa")
note2 = Note ("wino", "kurwa")
notebook1 = Notebook()
notebook1.new_note(note1)
notebook1.new_note(note1)
notebook1.new_note(note1)
notebook1.new_note(note2)

print(notebook1)



