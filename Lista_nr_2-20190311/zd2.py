import math
class Figure:
    def __init__(self):
        self.colour = "red"
        self.is_filled = False
    def __str__(self):
        return f"{self.colour} and {self.is_filled} \n"
    def __repr__(self):
        dict = self.__dict__()
        text =""
        for arg in dict:
            text+= str(dict[arg]) + " "
        return text

    def __dict__(self):
        dict = {}
        dict["self.colour"] = self.colour
        dict["self.is_filled"] = self.is_filled
        return dict

class Circle(Figure):
    def __init__(self, radius):
        super().__init__()
        self.radius = radius

    @property
    def radius(self):
        return self._radius
    @radius.setter
    def radius(self, value):
        try:
            float(value)
        except ValueError:
            print(f"{value} nie jest liczba \n")
        else:
            if value <= 0:
                print(f"{value} zły zakres")
            else:
                self._radius = value
    def area(self):
        return math.pi * self._radius ** 2
    def __str__(self):
        return f"{super().__str__()} {self._radius} \n"

    def __repr__(self):
        text = super().__repr__()
        return text

    def __dict__(self):
        dict = super().__dict__()
        dict["self._radius"] = self._radius
        return dict

figure1 = Figure()
circle1 = Circle(21)
print(repr(figure1))
print(repr(circle1))

