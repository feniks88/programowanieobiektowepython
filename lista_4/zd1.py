class Pupil:

    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
        self.marks = {}
    def complete_marks(self):
        course = input("Podaj nazwe kursu ")
        print(course)
        while len(course) < 3 or not course.isalpha():
            course = input("Zly ciag tekstowy ")
        grades = ["1","1.0", "1.5", "2", "2.0","2.5", "3","3.0", "3.5", "4","4.0", "4.5", "5","5.0", "5.5", "6", "6.0"]
        grade = input("Podaj ocene ")
        while grade not in grades:
            grade = input("Ocena z poza zakresu ")
        self.marks[course] = grade

    def print_marks(self):
        for key, value in self.marks.items():
            print(f"Przedmiot: {key} z ocena {value}")

    def mean(self):
        sum = 0.0
        for key, value in self.marks.items():
            sum += float(value)
        return sum / len(self.marks)

    def __str__(self):
        return f"Imię i nazwisko: {self.name} {self.surname} Średnia ocen: {str(self.mean())}"

    @property
    def name(self):
        return self._name

    @property
    def surname(self):
        return self._surname

    @name.setter
    def name(self, value):
        while len(value) < 3 or not value.isalpha():
            value = input("Zly ciag tekstowy ")

        self._name = value

    @surname.setter
    def surname(self, value):
        while len(value) < 3 or not value.isalpha():
            value = input("Zle nazwisko ")
        self._surname = value


class Student(Pupil):
    def __init__(self, name, surname):
        super().__init__(name, surname)
        self.weights = {}
    def complete_weights(self):
        course = input("Podaj nazwe kursu by przypisac wage ")
        while len(course) < 3 or not (course in self.marks):
            course = input("Zly ciag tekstowy lub nie ma takiego przedmiotu ")
        weight = input("Podaj wage ")
        while 0.0 > float(weight) or float(weight) > 1.0:
            weight = input("Waga z poza zakresu ")
        self.weights[course] = weight
    def mean(self):
        sum = 0.0
        weights = 0.0
        for key, value in self.weights.items():
            weights+= float(value)
            sum += float(self.marks[key]) * float(value)
        return sum / weights

    def __str__(self):
        return str(super().__str__())

tmp2 = Student("bbb", "asfafaf")
tmp2.complete_marks()
tmp2.complete_weights()
tmp2.complete_marks()
tmp2.complete_weights()
tmp = Pupil("aaa", "ssadas")
tmp.complete_marks()
tmp.complete_marks()
print(tmp)
print(tmp2)

