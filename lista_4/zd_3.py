
from _collections_abc import Sequence
from abc import abstractmethod


class Sortedlist (Sequence):

    @abstractmethod
    def __getitem__(self, i: int):
        pass
    @abstractmethod
    def __getitem__(self, s: slice):
        pass
    def __getitem__(self, i: int):
        pass
    def __len__(self) -> int:
        pass

    def __init__(self, unsortedList, reverse=False, key=None):
        self.unsortedList = unsortedList
        self.reverse = reverse
        self.key = key
        if key is None:
            self.unsortedList.sort(reverse=self.reverse)
        else:
            self.unsortedList.sort(key = self.key, reverse=self.reverse)

    def index(self, value):
        mid = int(len(self.unsortedList)/2)
        index = -1
        if self.reverse == False:
            while index < 0 and mid >= 0:
                print(mid)
                if value == self.unsortedList[mid]:
                    index = mid
                elif value > self.unsortedList[mid]:
                    mid = mid + 1
                else:
                    mid = mid - 1
        else:
            while index < 0 and mid >= 0:
                print(mid)
                if value == self.unsortedList[mid]:
                    index = mid
                elif value < self.unsortedList[mid]:
                    mid = mid + 1
                else:
                    mid = mid - 1
        return index


listaa = [1,2,2,2,3,4,5]

a = Sortedlist(listaa)

print(a._find_index(2))