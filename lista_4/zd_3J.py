
from _collections_abc import Sequence
from abc import abstractmethod


class Sortedlist (Sequence):
    def __getitem__(self, i: int):
        return True

    def __len__(self) -> int:
        return len(self.sortedList)

    def __init__(self, unsortedList, key, reverse=False):
        #we funkcji Sortedlist zdefiniowana jest metoda init,
        #która przyjmuje nieposortowaną listę, nazwę funkcji sortującej i parametr revers
        self.reverse = reverse
        if reverse == True:
            self.sortedList = key(unsortedList)[::-1]# wywołanie funkcji sortującej z naszą nieposortowaną listą
                                                        # [::-1] -> składania do odwrócenia kolejności obiektów w liście
        else:
            self.sortedList = key(unsortedList)

    def _find_index(self, value):
        mid = int(len(self.sortedList)/2)
        index = -1
        #podzieliłem szukanie binarne na dwie funkcje dla liczb i stringów
        if isinstance(value, float) or isinstance(value, int):
            return self.binary_search_for_number(value, mid, index)
        if isinstance(value, str):
            return self.binary_search_for_string(value, mid, index)

    def binary_search_for_number(self, value, mid, index):

        if self.reverse == False:
            while index < 0 and mid >= 0:
                if value == self.sortedList[mid]:
                    index = mid
                elif value > self.sortedList[mid]:
                    mid = mid + 1
                else:
                    mid = mid - 1
        else:
            while index < 0 and mid >= 0:
                if value == self.sortedList[mid]:
                    index = mid
                elif value < self.sortedList[mid]:
                    mid = mid + 1
                else:
                    mid = mid - 1
        return index

    def binary_search_for_string(self, value, mid, index):
        if self.reverse == False:
            while index < 0 and mid >= 0:
                if value == self.sortedList[mid]:
                    index = mid
                elif len(value) > len(self.sortedList[mid]):
                    mid = mid + 1
                else:
                    mid = mid - 1
        else:
            while index < 0 and mid >= 0:
                if value == self.sortedList[mid]:
                    index = mid
                elif len(value) < len(self.sortedList[mid]):
                    mid = mid + 1
                else:
                    mid = mid - 1
        return index

#kod quicksorta podjebany ze stacka i edytowany, by działał też dla stringów
def quicksort(xs):
    if xs:
        if isinstance(xs[0], float) or isinstance(xs[0], int):
            below = [i for i in xs[1:] if i < xs[0]]
            above = [i for i in xs[1:] if i >= xs[0]]
            return quicksort(below) + [xs[0]] + quicksort(above)
        if isinstance(xs[0], str):
            below = [i for i in xs[1:] if len(i) < len(xs[0])]
            above = [i for i in xs[1:] if len(i) >= len(xs[0])]
            return quicksort(below) + [xs[0]] + quicksort(above)
    else:
        return xs

#testy : - DDDDDDD
listaa = [1,2,2,2,3,4,5]
listaa2 = [1.0, 2.0, 2.0, 2.0, 3.0, 4.0, 5.0]
listaa1 = ["jan", "paweł", "drugi", "małe dzieci : - DDD"]
a = Sortedlist(listaa, quicksort, True)
a1 = Sortedlist(listaa, quicksort)
b = Sortedlist(listaa1, quicksort)
c = Sortedlist(listaa, quicksort)
c1 = Sortedlist(listaa1, quicksort, True)

print(a.sortedList, "[float] znajdź index 5", a._find_index(5.0))
print(a1.sortedList,"[float] znajdź index 5", a1._find_index(5.0))
print(a.sortedList, "[int] znajdź index 2 ", a._find_index(2))
print(b.sortedList, "[str] znajdź index paweł", b._find_index("paweł"))
print(c1.sortedList,"[str] znajdź index paweł", c1._find_index("paweł"))