
from _collections_abc import Sequence
from abc import abstractmethod


class Sortedlist (Sequence):

    def __init__(self, unsortedList, key, reverse=False):
        self.key = key
        self.reverse = reverse
        self.sortedList = key(unsortedList)

    def __getitem__(self, i: int):
        return self.sortedList[i]

    def __len__(self) -> int:
        return len(self.sortedList)

    def __str__(self):
        return f"Posortowana lista: + {self.sortedList}"

    def _find_index(self, value, start, end):
        if self.sortedList == []:
            return -1
        if len(self.sortedList) > 1:
            if isinstance(value, float) or isinstance(value, int):
                return self.binary_search_for_number(value, start, end)
            if isinstance(value, str):
                return self.binary_search_for_string(value, start, end)
        if len(self.sortedList) == 1:
            return self.sortedList[0]

    def binary_search_for_number(self, value, start, end):
        sequence = self.sortedList
        lo, hi = start, end - 1
        while lo <= hi:
            mid = (lo + hi) // 2
            if sequence[mid] < value:
                lo = mid + 1
            elif value < sequence[mid]:
                hi = mid - 1
            else:
                return mid
        return -1

    def binary_search_for_string(self, value, start, end):
        sequence = self.sortedList
        lo, hi = start, end - 1
        while lo <= hi:
            mid = (lo + hi) // 2
            if len(sequence[mid]) < len(value):
                lo = mid + 1
            elif len(value) < len(sequence[mid]):
                hi = mid - 1
            else:
                return mid
        return -1

    def add(self, value):
        f_value = self._find_index(value, 0, len(self.sortedList))
        if f_value < 0:
            self.sortedList = self.sortedList + [value]
            self.sortedList = self.key(self.sortedList)
        else:
            self.sortedList = self.sortedList[:f_value] + [value] + self.sortedList[f_value:]

    def copy(self):
        return [i for i in self.sortedList]

    def clear(self):
        self.sortedList = []

    def extend(self, list):
        self.sortedList = self.key(self.sortedList + list)

    def count(self, value):
        return sum(1 for i in self.sortedList if i == value)

    def insert(self, index, lista):
        #niepotrzebna dodaje w konkretnym miejscu, a lista ma być posortowana
        self.sortedList = self.key(self.sortedList[:index] + lista + self.sortedList[index:])

    def remove(self, value):
        self.sortedList = [i for i in self.sortedList if i != value]

    def pop(self, item):
        return self.sortedList[item]




#kod quicksorta podjebany ze stacka i edytowany, by działał też dla stringów
def quicksort(xs):
    if xs:
        if isinstance(xs[0], float) or isinstance(xs[0], int):
            below = [i for i in xs[1:] if i < xs[0]]
            above = [i for i in xs[1:] if i >= xs[0]]
            return quicksort(below) + [xs[0]] + quicksort(above)
        if isinstance(xs[0], str):
            below = [i for i in xs[1:] if len(i) < len(xs[0])]
            above = [i for i in xs[1:] if len(i) >= len(xs[0])]
            return quicksort(below) + [xs[0]] + quicksort(above)
    else:
        return xs

'''#testy : - DDDDDDD
listaa = [1]
listaa2 = [1.0, 2.0, 2.0, 2.0, 3.0, 4.0, 5.0]
listaa1 = ["jan", "paweł", "drugi", "małe dzieci : - DDD"]
a = Sortedlist(listaa2, quicksort, True)
a1 = Sortedlist(listaa, quicksort)
b = Sortedlist(listaa1, quicksort)
c = Sortedlist(listaa, quicksort)
c1 = Sortedlist(listaa1, quicksort, True)


print(b.sortedList, "[str] znajdź index drugi", b._find_index("drugi", 0, len(b.sortedList)))
b.add("dawidddd")
print(b.sortedList, "[str] znajdź index paweł", b._find_index("paweł", 0, len(b.sortedList)))

print(c1.sortedList,"[str] znajdź index paweł", b._find_index("paweł", 0, len(c1.sortedList)))
x = b.copy()
b.clear()
b.extend(["jebanie"])
print(b.sortedList)
print(x)

b.extend(x)
print(b.sortedList)
b.extend(x)
print(b.sortedList)
b.insert(2, ["kurwa"])
print(b.sortedList)
b.remove("kurwa")
print(b.sortedList)'''